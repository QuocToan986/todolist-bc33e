import { getEle, renderTask } from "./js/controller.js";
import { ToDoList } from "./model/model.js";

let BASE_URL = "https://63063e00c0d0f2b801191139.mockapi.io";

let todolist = [];

let renderListTaskService = () => {
  axios({
    url: `${BASE_URL}/todolist`,
    method: "GET",
  })
    .then((res) => {
      todolist = res.data;
      renderTask(todolist);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
//gọi AIP
renderListTaskService();

//btn done
let btnDone = (id) => {
  axios({
    url: `${BASE_URL}/todolist/${id}`,
    method: "GET",
  })
    .then((res) => {
      let task = res.data;
      console.log("task: ", task);
      if (task.check) {
        axios({
          url: `${BASE_URL}/todolist/${id}`,
          method: "PUT",
          data: { check: false },
        }).then((res) => {
          console.log(res.data);
          renderListTaskService();
        });
      } else {
        axios({
          url: `${BASE_URL}/todolist/${id}`,
          method: "PUT",
          data: { check: true },
        }).then((res) => {
          renderListTaskService();
        });
      }
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
window.btnDone = btnDone;

// btn xóa
let btnRemove = (id) => {
  axios({
    url: `${BASE_URL}/todolist/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      renderListTaskService();
    })
    .catch((err) => {});
};
window.btnRemove = btnRemove;

// btn thêm
getEle("addItem").addEventListener("click", function () {
  let newTask = getEle("newTask").value;
  let data = new ToDoList(newTask, false, 0);
  axios({
    url: `${BASE_URL}/todolist`,
    method: "POST",
    data: data,
  })
    .then((res) => {
      renderListTaskService();
    })
    .catch((err) => {
      console.log(err);
    });
});

// sort a-z
getEle("two").addEventListener("click", function () {
  axios({
    url: `${BASE_URL}/todolist?sortBy=toDo&order=asc`,
    method: "GET",
  })
    .then((res) => {
      console.log("res: ", res.data);
      renderTask(res.data);
    })
    .catch((err) => {});
});

// sort a-z
getEle("three").addEventListener("click", function () {
  axios({
    url: `${BASE_URL}/todolist?sortBy=toDo&order=desc`,
    method: "GET",
  })
    .then((res) => {
      console.log("res: ", res.data);
      renderTask(res.data);
    })
    .catch((err) => {});
});
