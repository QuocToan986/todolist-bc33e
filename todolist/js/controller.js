// dom
export let getEle = (id) => {
  return document.getElementById(id);
};

// render
export let renderTask = (data) => {
  let contentTodo = "";
  let contentCompleted = "";
  data.forEach((element) => {
    if (element.check) {
      contentCompleted += /*html*/ `
  <li>
    <p>${element.toDo}</p>
    <p>
    <span class = "todo" onclick = "btnDone(${element.id})"><i class="fa fa-check-circle"></i></span>
    <span onclick = "btnRemove(${element.id})"><i class="fa fa-trash bin"></i></span>
    </p>
  </li>
  `;
    } else {
      contentTodo += /*html*/ `
    <li>
      <p>${element.toDo}</p>
      <p>
      <span class = "todo" onclick = "btnDone(${element.id})"><i class="fa fa-check-circle"></i></span>
      <span onclick = "btnRemove(${element.id})"><i class="fa fa-trash bin"></i></span>
      </p>
    </li>
    `;
    }
  });
  getEle("completed").innerHTML = contentCompleted;
  getEle("todo").innerHTML = contentTodo;
};
let content = (element) => {
  getEle("todo").innerHTML += content;
};

// tìm index
export let findIndex = (list, id) => {
  for (let index = 0; index < list.length; index++) {
    const element = list[index];
    if (element.id == id) {
      return index;
    }
  }
  return -1;
};
